
#Imports
import minetweaker.item.IItemStack;
import minetweaker.data.IData;
import mods.agricraft.SeedMutation;
import mods.agricraft.growing.Soil;
import mods.agricraft.growing.FertileSoils;
import mods.agricraft.growing.BaseBlock;
import mods.agricraft.CropProduct;

#Values
	#Magical Crops
	val Minicio = <magicalcrops:magicalcrops_1MinicioEssence>;
	val Accio = <magicalcrops:magicalcrops_2AccioEssence>;
	val Crucio = <magicalcrops:magicalcrops_3CrucioEssence>;
	val Imperio = <magicalcrops:magicalcrops_4ImperioEssence>;
	val Zivicio = <magicalcrops:magicalcrops_5ZivicioEssence>;
	val CoalSeeds = <magicalcrops:magicalcrops_CoalSeeds>;
	val DyeSeeds = <magicalcrops:magicalcrops_DyeSeeds>;
	val EarthSeeds = <magicalcrops:magicalcrops_EarthSeeds>;
	val FireSeeds = <magicalcrops:magicalcrops_FireSeeds>;
	val MinicioSeeds = <magicalcrops:magicalcrops_MinicioSeeds>;
	val NatureSeeds = <magicalcrops:magicalcrops_NatureSeeds>;
	val WaterSeeds = <magicalcrops:magicalcrops_WaterSeeds>;
	val RedstoneSeeds = <magicalcrops:magicalcrops_RedstoneSeeds>;
	val GlowstoneSeeds = <magicalcrops:magicalcrops_GlowstoneSeeds>;
	val ObsidianSeeds = <magicalcrops:magicalcrops_ObsidianSeeds>;
	val NetherSeeds = <magicalcrops:magicalcrops_NetherSeeds>;
	val IronSeeds = <magicalcrops:magicalcrops_IronSeeds>;
	val GoldSeeds = <magicalcrops:magicalcrops_GoldSeeds>;
	val LapisSeeds = <magicalcrops:magicalcrops_LapisSeeds>;
	val ExperienceSeeds = <magicalcrops:magicalcrops_ExperienceSeeds>;
	val DiamondSeeds = <magicalcrops:magicalcrops_DiamondSeeds>;   
	val EmeraldSeeds = <magicalcrops:magicalcrops_EmeraldSeeds>;
	val BlazeSeeds = <magicalcrops:magicalcrops_BlazeSeeds>;
	val CreeperSeeds = <magicalcrops:magicalcrops_CreeperSeeds>;
	val EndermanSeeds = <magicalcrops:magicalcrops_EndermanSeeds>;
	val PigSeeds = <magicalcrops:magicalcrops_PigSeeds>;
	val PigEssence = <magicalcrops:magicalcrops_PigEssence>;
	val SheepEssence = <magicalcrops:magicalcrops_SheepEssence>;
	val SheepSeeds = <magicalcrops:magicalcrops_SheepSeeds>;
	val CowSeeds = <magicalcrops:magicalcrops_CowSeeds>;
	val CowEssence = <magicalcrops:magicalcrops_CowSeeds>;
	val GhastSeeds = <magicalcrops:magicalcrops_GhastSeeds>;
	val SkeletonSeeds = <magicalcrops:magicalcrops_SkeletonSeeds>;
	val SlimeSeeds = <magicalcrops:magicalcrops_SlimeSeeds>;
	val SpiderSeeds = <magicalcrops:magicalcrops_SpiderSeeds>;
	val WitherSeeds = <magicalcrops:magicalcrops_WitherSeeds>;
	val ChickenSeeds = <magicalcrops:magicalcrops_ChickenSeeds>;
	val ChickenEssence = <magicalcrops:magicalcrops_ChickenEssence>;
	val LapisEssence = <magicalcrops:magicalcrops_LapisSeeds>;
	val RedstoneEssence = <magicalcrops:magicalcrops_RedstoneEssence>;
	val DiamondEssence = <magicalcrops:magicalcrops_DiamondEssence>;
	val QuartzSeeds = <magicalcrops:magicalcrops_QuartzSeeds>;
	
	#Thermal Expansion
	val TinSeeds = <magicalcrops:magicalcrops_TinSeeds>;
	val CopperSeeds = <magicalcrops:magicalcrops_CopperSeeds>;
	val SilverSeeds = <magicalcrops:magicalcrops_SilverSeeds>;
	val FerrousSeeds = <magicalcrops:magicalcrops_NickelSeeds>;
	val LeadSeeds = <magicalcrops:magicalcrops_LeadSeeds>;
	val FerrousEssence = <magicalcrops:magicalcrops_NickelEssence>;

	#Tinkers Construct
	val CobaltSeeds = <magicalcrops:magicalcrops_CobaltSeeds>;
	val ArditeSeeds = <magicalcrops:magicalcrops_ArditeSeeds>;

	#AE2
	val CertusQuartzSeed = <magicalcrops:magicalcrops_CertusQuartzSeeds>;
	val CertusQuartzEssence = <magicalcrops:magicalcrops_CertusQuartzEssence>;
	
	#Agricraft
	val seedWheat = <minecraft:wheat_seeds>;
	val seedCarrot = <AgriCraft:seedCarrot>;
	val seedPotato = <AgriCraft:seedPotato>;

#Rename
seedWheat.displayName = "Wheat Seeds";
FerrousSeeds.displayName = "Ferrous Seeds";
FerrousEssence.displayName = "Ferrous Essence";

#Remove
recipes.remove(<magicalcrops:magicalcrops_InfusionStoneWeak>);
recipes.remove(<magicalcrops:magicalcrops_InfusionStoneRegular>);
recipes.remove(<magicalcrops:magicalcrops_InfusionStoneStrong>);
recipes.remove(<magicalcrops:magicalcrops_InfusionStoneExtreme>);
recipes.remove(<magicalcrops:magicalcrops_InfusionStoneMaster>);
recipes.remove(CoalSeeds);
recipes.remove(CertusQuartzSeed);
recipes.remove(IronSeeds);
recipes.remove(RedstoneSeeds);
recipes.remove(GoldSeeds);
recipes.remove(DiamondSeeds);
recipes.remove(EmeraldSeeds);
recipes.remove(TinSeeds);
recipes.remove(CopperSeeds);
recipes.remove(SilverSeeds);
recipes.remove(LeadSeeds);
recipes.remove(FerrousSeeds);
recipes.remove(CobaltSeeds);
recipes.remove(ArditeSeeds);       

#Add
recipes.addShaped(MinicioSeeds, [[null, <magicalcrops:magicalcrops_1MinicioEssence>, null], 
                                                             [<magicalcrops:magicalcrops_1MinicioEssence>, <minecraft:wheat_seeds>, <magicalcrops:magicalcrops_1MinicioEssence>], 
															 [null, <magicalcrops:magicalcrops_1MinicioEssence>, null]]);

recipes.addShaped(<magicalcrops:magicalcrops_InfusionStoneWeak>, [[<magicalcrops:magicalcrops_1MinicioEssence>, <magicalcrops:magicalcrops_1MinicioEssence>, <magicalcrops:magicalcrops_1MinicioEssence>],
                                                                  [<magicalcrops:magicalcrops_1MinicioEssence>, <minecraft:cobblestone>, <magicalcrops:magicalcrops_1MinicioEssence>], 
                                                                  [<magicalcrops:magicalcrops_1MinicioEssence>, <magicalcrops:magicalcrops_1MinicioEssence>, <magicalcrops:magicalcrops_1MinicioEssence>]]);

recipes.addShaped(<magicalcrops:magicalcrops_InfusionStoneStrong>, [[<magicalcrops:magicalcrops_3CrucioEssence>, <magicalcrops:magicalcrops_3CrucioEssence>, <magicalcrops:magicalcrops_3CrucioEssence>], [<magicalcrops:magicalcrops_3CrucioEssence>, <minecraft:cobblestone>, <magicalcrops:magicalcrops_3CrucioEssence>], [<magicalcrops:magicalcrops_3CrucioEssence>, <magicalcrops:magicalcrops_3CrucioEssence>, <magicalcrops:magicalcrops_3CrucioEssence>]]);

recipes.addShaped(<magicalcrops:magicalcrops_InfusionStoneExtreme>, [[<magicalcrops:magicalcrops_4ImperioEssence>, <magicalcrops:magicalcrops_4ImperioEssence>, <magicalcrops:magicalcrops_4ImperioEssence>],  
                                                                     [<magicalcrops:magicalcrops_4ImperioEssence>, <minecraft:cobblestone>, <magicalcrops:magicalcrops_4ImperioEssence>], 
                                                                     [<magicalcrops:magicalcrops_4ImperioEssence>, <magicalcrops:magicalcrops_4ImperioEssence>, <magicalcrops:magicalcrops_4ImperioEssence>]]);

recipes.addShaped(<magicalcrops:magicalcrops_InfusionStoneMaster>, [[<magicalcrops:magicalcrops_5ZivicioEssence>, <magicalcrops:magicalcrops_5ZivicioEssence>, <magicalcrops:magicalcrops_5ZivicioEssence>],                        
                                                                    [<magicalcrops:magicalcrops_5ZivicioEssence>, <minecraft:cobblestone>, <magicalcrops:magicalcrops_5ZivicioEssence>], 
                                                                    [<magicalcrops:magicalcrops_5ZivicioEssence>, <magicalcrops:magicalcrops_5ZivicioEssence>, <magicalcrops:magicalcrops_5ZivicioEssence>]]);

#Mutations
//Carrot Seeds
SeedMutation.add(seedCarrot, seedWheat, seedWheat);

//Coal Seeds
SeedMutation.add(CoalSeeds, MinicioSeeds, seedPotato);

//Certus Quartz Seeds
SeedMutation.add(CertusQuartzSeed, CoalSeeds, QuartzSeeds);

//Quartz Seeds
SeedMutation.add(QuartzSeeds, CoalSeeds, seedCarrot);

//Iron Seeds
SeedMutation.add(IronSeeds, CoalSeeds, QuartzSeeds);

//Lapis Seeds
SeedMutation.add(LapisSeeds, CertusQuartzSeed, IronSeeds);

//Redstone Seeds
SeedMutation.add(RedstoneSeeds, LapisSeeds, IronSeeds);

//Gold Seeds
SeedMutation.add(GoldSeeds, RedstoneSeeds, LapisSeeds);

//Diamond Seeds
SeedMutation.add(DiamondSeeds, GoldSeeds, RedstoneSeeds);

//Emerald Seeds
SeedMutation.add(EmeraldSeeds, DiamondSeeds, GoldSeeds);

//Tin Seeds
SeedMutation.add(TinSeeds, IronSeeds, CoalSeeds);

//Copper Seeds
SeedMutation.add(CopperSeeds, TinSeeds,CoalSeeds);

//Silver Seeds
SeedMutation.add (SilverSeeds, GoldSeeds, CoalSeeds);

//Lead Seeds
SeedMutation.add (LeadSeeds, IronSeeds, LapisSeeds);

//Ferrous Seeds
SeedMutation.add (FerrousSeeds, GoldSeeds, QuartzSeeds);

//Cobalt Seeds
SeedMutation.add (CobaltSeeds, EmeraldSeeds, CopperSeeds);

//Ardite Seeds
SeedMutation.add (ArditeSeeds, EmeraldSeeds, DiamondSeeds);

#Tooltips
Minicio.addTooltip(format.white("For") + " Pronunciation Press " + format.darkGreen("Shift"));

Accio.addTooltip(format.white("For") + " Pronunciation Press " + format.gold("Shift"));

Crucio.addTooltip(format.white("For") + " Pronunciation Press " + format.yellow("Shift"));

Imperio.addTooltip(format.white("For") + " Pronunciation Press " + format.blue("Shift"));

Zivicio.addTooltip(format.white("For") + " Pronunciation Press " + format.lightPurple("Shift"));

CoalSeeds.addTooltip(format.white("For") + " Uses Press " + format.darkGray("Shift"));

DyeSeeds.addTooltip(format.white("For") + " Uses Press " + format.aqua("Shift"));

EarthSeeds.addTooltip(format.white("For") + " Uses Press " + format.darkGreen("Shift"));

FireSeeds.addTooltip(format.white("For") + " Uses Press " + format.darkRed("Shift"));

MinicioSeeds.addTooltip(format.white("For") + " Uses Press " + format.green("Shift"));

NatureSeeds.addTooltip(format.white("For") + " Uses Press " + format.green("Shift"));

WaterSeeds.addTooltip(format.white("For") + " Uses Press " + format.aqua("Shift"));

RedstoneSeeds.addTooltip(format.white("For") + " Uses Press " + format.darkRed("Shift"));

GlowstoneSeeds.addTooltip(format.white("For") + " Uses Press " + format.yellow("Shift"));

ObsidianSeeds.addTooltip(format.white("For") + " Uses Press " + format.darkPurple("Shift"));

NetherSeeds.addTooltip(format.white("For") + " Uses Press " + format.darkRed("Shift"));

IronSeeds.addTooltip(format.white("For") + " Uses Press " + format.gray("Shift"));

GoldSeeds.addTooltip(format.white("For") + " Uses Press " + format.gold("Shift"));

LapisSeeds.addTooltip(format.white("For") + " Uses Press " + format.darkBlue("Shift"));

ExperienceSeeds.addTooltip(format.white("For") + " Uses Press " + format.green("Shift"));

QuartzSeeds.addTooltip(format.white("For") + " Uses Press " + format.white("Shift"));

DiamondSeeds.addTooltip(format.white("For") + " Uses Press " + format.aqua("Shift"));

EmeraldSeeds.addTooltip(format.white("For") + " Uses Press " + format.green("Shift"));

BlazeSeeds.addTooltip(format.white("For") + " Uses Press " + format.darkRed("Shift"));

CreeperSeeds.addTooltip(format.white("For") + " Uses Press " + format.green("Shift"));

EndermanSeeds.addTooltip(format.white("For") + " Uses Press " + format.darkGray("Shift"));

PigSeeds.addTooltip(format.white("For") + " Uses Press " + format.lightPurple("Shift"));

SheepSeeds.addTooltip(format.white("For") + " Uses Press " + format.gray("Shift"));

CowSeeds.addTooltip(format.white("For") + " Uses Press " + format.green("Shift"));

GhastSeeds.addTooltip(format.white("For") + " Uses Press " + format.gray("Shift"));

SkeletonSeeds.addTooltip(format.white("For") + " Uses Press " + format.gray("Shift"));

SlimeSeeds.addTooltip(format.white("For") + " Uses Press " + format.darkGreen("Shift"));

SpiderSeeds.addTooltip(format.white("For") + " Uses Press " + format.darkRed("Shift"));

WitherSeeds.addTooltip(format.white("For") + " Uses Press " + format.gray("Shift"));

ChickenSeeds.addTooltip(format.white("For") + " Uses Press " + format.gold("Shift"));

#Shift-Tooltips
Minicio.addShiftTooltip(format.darkGreen("Min-ee-key-o"));

Accio.addShiftTooltip(format.gold("Ak-ee-o"));

Crucio.addShiftTooltip(format.yellow("Krew-she-o"));

Imperio.addShiftTooltip(format.blue("Imp-er-ee-o"));

Zivicio.addShiftTooltip(format.lightPurple("Ziv-is-ee-o"));

CoalSeeds.addShiftTooltip(format.darkGray("Grows Essence Of Coal"));

DyeSeeds.addShiftTooltip(format.aqua("Grows Essence Of Dye"));

EarthSeeds.addShiftTooltip(format.darkGreen("Grows Essence Of Earth"));

FireSeeds.addShiftTooltip(format.darkRed("Grows Essence Of Fire"));

MinicioSeeds.addShiftTooltip(format.green("Grows Minicio Essence"));

NatureSeeds.addShiftTooltip(format.green("Grows Essence Of Nature"));

WaterSeeds.addShiftTooltip(format.aqua("Grows Essence Of Water"));

RedstoneSeeds.addShiftTooltip(format.darkRed("Grows Redstone Essence"));

GlowstoneSeeds.addShiftTooltip(format.yellow("Grows Glowstone Essence"));

ObsidianSeeds.addShiftTooltip(format.darkPurple("Grows Obsidian Essence"));

NetherSeeds.addShiftTooltip(format.darkRed("Grows Nether Essence"));

IronSeeds.addShiftTooltip(format.gray("Grows Iron Essence"));

GoldSeeds.addShiftTooltip(format.gold("Grows Gold Essence"));

LapisSeeds.addShiftTooltip(format.darkBlue("Grows Lapis Lazuli Essence"));

ExperienceSeeds.addShiftTooltip(format.green("Grows Experience Essence"));

DiamondSeeds.addShiftTooltip(format.aqua("Grows Diamond Essence"));

EmeraldSeeds.addShiftTooltip(format.green("Grows Emerald Essence"));

BlazeSeeds.addShiftTooltip(format.darkRed("Grows Blaze Essence"));

CreeperSeeds.addShiftTooltip(format.green("Grows Creeper Essence"));

EndermanSeeds.addShiftTooltip(format.darkGray("Grows Enderman Essence"));

PigSeeds.addShiftTooltip(format.lightPurple("Grows Pig Essence"));

SheepSeeds.addShiftTooltip(format.gray("Grows Sheep Essence"));

CowSeeds.addShiftTooltip(format.green("Grows Cow Essence"));

GhastSeeds.addShiftTooltip(format.gray("Grows Ghast Essence"));

SkeletonSeeds.addShiftTooltip(format.gray("Grows Skeleton Essence"));

SlimeSeeds.addShiftTooltip(format.darkGreen("Grows Slime Essence"));

SpiderSeeds.addShiftTooltip(format.darkRed("Grows Spider Essence"));

WitherSeeds.addShiftTooltip(format.gray("Grows Wither Essence"));

ChickenSeeds.addShiftTooltip(format.gold("Grows Chicken Essence"));
