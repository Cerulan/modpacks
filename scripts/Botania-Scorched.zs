import mods.botania.Apothecary;

Apothecary.removeRecipe("hydroangeas");
Apothecary.addRecipe("hydroangeas", [<ore:petalBlue>, <ore:petalBlue>, <ore:petalCyan>, <ore:petalCyan>, <ore:petalCyan>, <ore:petalPink>]);